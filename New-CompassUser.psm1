function Get-SecureStringAsPlainText
{
    [CmdletBinding()]
    [Alias('gssapt')]
    [OutputType([string])]
    param(
        [Parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$True)]     
        [SecureString]$pw 
    )
    process {        
       Return [System.Runtime.InteropServices.Marshal]::PtrToStringUni([System.Runtime.InteropServices.Marshal]::SecureStringToCoTaskMemUnicode($pw))              
    }
}

Function New-CompassUser {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName)]
        [alias("SussiId", "Username")]
        [string]$SamAccountName,
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName)]
        [alias("FirstName")]
        [string]$GivenName,
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName)]
        [alias("LastName")]
        [string]$Surname,
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$DateOfBirth = "01/01/1901",
        [Parameter(Mandatory = $false, ValueFromPipelineByPropertyName)]
        [alias("Email")]
        [string]$Mail,
        [Parameter(Mandatory = $false, ParameterSetName = "domain", ValueFromPipelineByPropertyName)]
        [string]$EmailDomain
    )

    begin {
        Import-Module "$PSScriptRoot\selenium-powershell\Selenium.psm1" -Force

        Write-Host "Enter your Compass login..."
        $username = (Read-Host -Prompt "Username")
        $password = (gssapt -pw (Read-Host -Prompt "Password" -AsSecureString))
        
        # $TextInfo is used in capitilizing the first letter of names, in conjunction with the method ToTitleCase()
        $TextInfo = (Get-Culture).TextInfo 

        $($Driver = Start-SeChrome) | Out-Null
    }

    process {
        If(!($_.mail)) {
            $Mail = (Get-ADUser $SamAccountName -Properties mail).mail
        }

        Enter-SeUrl "https://mckinnonsc-vic.compass.education/Organise/PeopleManagement/" -Driver $Driver
        
        "username", "password" | ForEach-Object {
            Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Id $_) -Keys $(Get-Variable -Name "$_").Value
        }
        
        $webDriverWait = New-Object OpenQA.Selenium.Support.UI.WebDriverWait($Driver, $(New-TimeSpan -Seconds 5))
        $webDriverWait.Until([OpenQA.Selenium.Support.UI.ExpectedConditions]::ElementToBeClickable([OpenQA.Selenium.By]::Id("button1"))) | Out-Null
        Invoke-SeClick -Element $(Find-SeElement -Driver $Driver -Id "button1")
        
        $webDriverWait.Until([OpenQA.Selenium.Support.UI.ExpectedConditions]::ElementToBeClickable([OpenQA.Selenium.By]::Id("button-1062"))) | Out-Null
        Invoke-SeClick -Element $(Find-SeElement -Driver $Driver -Id "button-1062")
        $webDriverWait.Until([OpenQA.Selenium.Support.UI.ExpectedConditions]::ElementToBeClickable([OpenQA.Selenium.By]::Id("menuitem-1065-itemEl"))) | Out-Null
        Invoke-SeClick -Element $(Find-SeElement -Driver $Driver -Id "menuitem-1065-itemEl")
        
        $webDriverWait.Until([OpenQA.Selenium.Support.UI.ExpectedConditions]::ElementIsVisible([OpenQA.Selenium.By]::Name("importIdentifier"))) | Out-Null
        Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "importIdentifier") -Keys $SamAccountName
        Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "firstName") -Keys $TextInfo.ToTitleCase($GivenName)
        Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "lastName") -Keys $TextInfo.ToTitleCase($Surname)
        Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "dob") -Keys $DateOfBirth
        
        If ($Mail) {
            Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "email") -Keys $Mail.ToLower()
        } Else {
            Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "email") -Keys ("$SamAccountName@$EmailDomain").ToLower()
        }
        
        Invoke-SeClick -Element $(Find-SeElement -Driver $Driver -Id "tab-1635-btnInnerEl")
        $webDriverWait.Until([OpenQA.Selenium.Support.UI.ExpectedConditions]::ElementIsVisible([OpenQA.Selenium.By]::Name("sussiId"))) | Out-Null
        Send-SeKeys -Element $(Find-SeElement -Driver $Driver -Name "sussiId") -Keys $SamAccountName
        
        # Save!
        #Invoke-SeClick -Element $(Find-SeElement -Driver $Driver -Id "button-1506-btnInnerEl")
    }

    end {}
}
