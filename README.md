# New-CompassUser

Powershell module to allow creating local Compass users. Accepts pipeline input from New-ADUser

## Requires
selenium-powershell https://github.com/adamdriscoll/selenium-powershell

## Setup
Download and extract selenium-powershell to the folder selenium-powershell

## Usage
Get-ADUser "john.smith@contoso.com" | New-CompassUser